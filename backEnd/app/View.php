<?php
namespace app;


interface View {

    public static function getInstance($config, Container $container );
    public function run();
}