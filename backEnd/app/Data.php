<?php
namespace app;


interface Data {

    public static function getInstance($config = [], Container $container);

} 