<?php
namespace app\defaultClass;


use app\Container;

class View implements \app\View {

    private static $singleton;
    /**
     * @var array
     */
    private $config;
    /**
     * @var Container $container
     */
    private $container;

    private function get($string)
    {
        return isset($this->config[$string])?$this->config[$string]:null;
    }

    public function run()
    {
        echo '<pre>';

        $template = $this->container
            ->toolsFile()
            ->getFile($this->get('template'));

        $tags = $this->container
            ->toolsString()
            ->searchedAndPullingOutTags($template);

        $content = $this->container
            ->toolsString()
            ->insertStringByTags($template, array_flip($tags[1]));

        print_r($content);
    }
    /**
     * @param $config
     * @param Container $container
     * @return View
     */
    public static function getInstance($config, Container $container )
    {
        if(self::$singleton === null) {
            self::$singleton = new View(@$config, $container);
        }

        return self::$singleton;
    }

    private function __construct($config = [], Container $container)
    {
        $this->config = $config;
        $this->container = $container;

    }

    private function __clone() {}

}