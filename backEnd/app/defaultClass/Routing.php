<?php
namespace app\defaultClass;

use app\Container;

class Routing implements \app\Routing {

    private static $singleton;
    /**
     * @var array $config
     */
    private $config;
    /**
     * @var Container $container
     */
    private $container;

    public static function getInstance($config = [], Container $container)
    {
        if(self::$singleton === null) {
            self::$singleton = new Routing($config, $container);
        }
        return self::$singleton;
    }

    /**
     * Routing constructor.
     * @param array $config
     * @param $container
     */
    private function __construct($config = [], Container $container)
    {
        $this->config = $config;
        $this->container = $container;
    }

    private function __clone() {}

    public function get( /*string*/ $string)
    {
        return 'hello Routing' .$string;
    }
}
