<?php
namespace app\defaultClass;

use app\Container;
use Content\ContentPage;

class Data implements \app\Data {

    private static $singleton;
    /**
     * @var Container
     */
    private $container;
    /**
     * @var array
     */
    private $config;
    private $data;

    public static function getInstance($config = [], Container $container)
    {
        if(self::$singleton === null) {
            self::$singleton = new Data($config, $container);
        }
        return self::$singleton;
    }

    private function __construct($config = [], Container $container)
    {
        $this->container = $container;
        $this->config = $config;
    }
    private function __clone() {}

}