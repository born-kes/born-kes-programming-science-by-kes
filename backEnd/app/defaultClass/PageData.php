<?php
namespace app\defaultClass;


use app\Container;

class PageData implements \app\PageData {

    /**
     * @var PageData
     */
    private static $singleton;
    /**
     * @var Container
     */
    private $container;
    /**
     * @var array
     */
    private $config;

    public static function getInstance($config = [], Container $container)
    {
        if(self::$singleton === null) {
            self::$singleton = new PageData($config, $container);
        }
        return self::$singleton;
    }

    private function __construct($config = [], Container $container)
    {
        $this->container = $container;
        $this->config = $config;
    }

    private function __clone() {}

} 