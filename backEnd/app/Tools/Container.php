<?php
namespace app\Tools;

use app\defaultClass\Data;
use app\defaultClass\PageData;
use app\defaultClass\Routing;
use app\defaultClass\View;

class Container implements \app\Container {

    /** @var  Container $singleton */
    private static $singleton;
    /** @var  ToolsFile */
    private $toolsFile;
    /** @var  ToolsString */
    private $toolsString;
    /** @var Routing */
    private $routing;
    /** @var  PageData */
    private $pageData;
    /** @var  Data */
    private $data;
    /** @var View */
    private $view;
    /** @var  $GLOBALS */
    private $globals;
    /** @var  $_SERVER */
    private $server;

    public static function getInstance()
    {
        if(self::$singleton === null) {
            self::$singleton = new Container();
        }

        return self::$singleton;
    }
    public static function set(ContainerBuilder $ContainerBuilder){
        self::$singleton->setValue($ContainerBuilder);
    }

    private function setValue(ContainerBuilder $ContainerBuilder)
    {
        $this->toolsFile = $ContainerBuilder->get('toolsFile');
        $this->toolsString = $ContainerBuilder->get('toolsString');
        $this->routing = $ContainerBuilder->get('routing');
        $this->pageData = $ContainerBuilder->get('pageData');
        $this->data = $ContainerBuilder->get('data');
        $this->view = $ContainerBuilder->get('view');
        $this->globals = $ContainerBuilder->get('globals');
        $this->server = $ContainerBuilder->get('server');
    }

    /**
     * @return ToolsFile
     */
    public function toolsFile ()
    {
        return $this->toolsFile;
    }

    /**
     * @return ToolsString
     */
    public function toolsString()
    {
        return $this->toolsString;
    }

    /**
     * @return Routing
     */
    public function routing()
    {
        return $this->routing;
    }

    /**
     * @return PageData
     */
    public function pageData()
    {
        return $this->pageData;
    }

    /**
     * @return Data
     */
    public function data()
    {
        return $this->data;
    }

    /**
     * @return View
     */
    public function view()
    {
        return $this->view;
    }

    /**
     * @return array $GLOBALS
     */
    public function globals()
    {
        return $this->globals;
    }

    /**
     * @return array $_SERVER
     */
    public function server()
    {
        return $this->server;
    }

    private function __clone()
    {
    }
    private function __construct()
    {
    }

}