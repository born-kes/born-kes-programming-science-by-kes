<?php
namespace app\Tools;


class Controller
{

    private $container;

    public function __construct(\app\Container $container)
    {
        $this->container = $container;
    }

    public function run()
    {
    }

}