<?php
namespace app\Tools;


class ToolsString {

    /**
     * Return array parameters from $str = param1=key.value
     *
     * @param string $string
     * @param string $regex
     * @param bool $empty
     *
     * @return array
     */
    public function splitStringByParameters(string $string, $regex = '/([=.;\r])/', $empty = FALSE)
    {
        return array_map('trim',
            preg_split($regex, $string, 0, $empty ? 0: PREG_SPLIT_NO_EMPTY)
        );
    }

    /**
     * @param string $string $string
     * @param string $regex
     * @return array
     */
    public function searchedAndPullingOutTags(/*string*/ $string, $regex = '/\{\{--(\w+)--\}\}/' )
    {
        $blocks=array();
        preg_match_all($regex, $string, $blocks );
        return $blocks;
    }

    /**
     * @param string $string $string
     * @param array $content
     * @return string|mixed
     */
    public function insertStringByTags(/*string*/ $string, array $content )
    {
        $blocks = self::searchedAndPullingOutTags($string);

        for($i=0; count($blocks[0])>$i;$i++)
        {
            if( isset($content[ $blocks[1][$i] ]) ) {
                $str = is_array ( $content[$blocks[1][$i]])?
                    implode (" ", $content[$blocks[1][$i]]):
                    $content[$blocks[1][$i]];

                $string = str_replace ( $blocks[0][$i], $str, $string );
            } else {
                $string = str_replace ( $blocks[0][$i], '', $string );
            }
        }

        return $string;

    }
} 