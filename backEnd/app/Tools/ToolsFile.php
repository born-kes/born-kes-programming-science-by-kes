<?php
namespace app\Tools;


class ToolsFile {

    /**
     * @param $file
     * @return string
     */
    public function getFile(/*string*/ $file)
    {
        if(file_exists($file. PHP) )
        {
            return file_get_contents($file. PHP);
        }
        return '';
    }

    /**
     * @param string $string $string
     *
     * @return bool
     */
    public function isJson(string $string)
    {
        return ((is_string($string) &&
            (is_object(json_decode($string)) ||
                is_array(json_decode($string))))) ? true : false;
    }

} 