<?php
namespace app\Tools;


use app\Data;
use app\PageData;
use app\Routing;
use app\View;

class ContainerBuilder {

    private $toolsFile;
    private $toolsString;
    private $routing;
    private $pageData;
    private $data;
    private $view;
    private $globals;
    private $server;

    public function build(){
        Container::set($this);
    }

  /*  public static function getInstance()
    {
        if(self::$singleton === null) {
            self::$singleton = new Container();
        }

        return self::$singleton;
    }*/

    /**
     * @param $Tools
     * @return $this
     */
    public function setToolsFile($Tools )
    {
        $this->toolsFile = $Tools;
        return $this;
    }

    /**
     * @param $Tools
     * @return $this
     */
    public function setToolsString($Tools )

    {
        $this->toolsString = $Tools;
        return $this;
    }

    /**
     * @param Routing $routing
     * @return $this
     */
    public function setRouting(Routing $routing)
    {
        $this->routing = $routing;
        return $this;
    }

    /**
     * @param PageData $pageData
     * @return $this
     */
    public function setPageData(PageData $pageData )
    {
        $this->pageData = $pageData;
        return $this;
    }

    /**
     * @param Data $data
     * @return $this
     */
    public function setData(Data $data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param View $view
     * @return $this
     */
    public function setView(View $view)
    {
        $this->view = $view;
        return $this;
    }

    /**
     * @param $globals
     * @return $this
     */
    public function setGlobals($globals )
    {
        $this->globals = $globals;
        return $this;
    }

    /**
     * @param $server
     * @return $this
     */
    public function setServer($server )
    {
        $this->server=$server ;
        return $this;
    }

    public function get($name){
        /** @var TYPE_NAME $name */
        return $this->$name;
    }

}