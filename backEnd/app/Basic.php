<?php
namespace app;


class Basic {

    private $settings = [
        'DATA'          => [
            'name'=>'app\defaultClass\Data',
        ],
        'PAGE_DATA'          => [
            'name'=>'app\defaultClass\PageData',
        ],
        'ROUTING'        => [
            'name'=>'app\defaultClass\Routing',
        ],
        'VIEW'          => [
            'name'=>'app\defaultClass\View',
            'config' => ['template' => 'template/default/index.tpl' ]
        ]
    ];
    private $container;
    private $controller;

    public function __construct($config = []) {
        $this->settings = array_merge(
            (array) $this->settings,
            (array) $config
        );

        $this->container = Tools\Container::getInstance();

        ( new Tools\ContainerBuilder($this->container) )
            ->setGlobals( $GLOBALS )
            ->setServer( $_SERVER )
            ->setToolsFile ( new Tools\ToolsFile() )
            ->setToolsString ( new Tools\ToolsString() )
            ->setRouting ( $this->getInstance('ROUTING') )
            ->setPageData ( $this->getInstance('PAGE_DATA') )
            ->setData ( $this->getInstance('DATA') )
            ->setView ( $this->getInstance('VIEW') )
            ->build();

        $this->controller = new Tools\Controller( $this->container );


    }

    public function run(){
        echo '<pre>';
       var_dump($this->container->view()->run());
        echo '</pre>';
        echo  ' hello word:';
    }

    private function getInstance($key)
    {
        $className = $this->settings[$key]['name'];
        $config = @$this->settings[$key]['config'];

            return $className::getInstance($config, $this->container );
    }

}