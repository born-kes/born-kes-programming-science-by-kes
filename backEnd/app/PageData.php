<?php
namespace app;


interface PageData {

    public static function getInstance($config = [], Container $container);
}