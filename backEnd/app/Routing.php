<?php
namespace app;


interface Routing {

    public static function getInstance ($config = [], Container $container);
    public function get ($string);
}