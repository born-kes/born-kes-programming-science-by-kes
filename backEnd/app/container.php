<?php
namespace app;


use app\Tools\ToolsFile;
use app\Tools\ToolsString;

interface Container {


    /**
     * @return ToolsFile
     */
    public function toolsFile ();
    /**
     * @return ToolsString
     */
    public function toolsString ();
    /**
     * @return \app\defaultClass\Routing
     */
    public function routing ();
    /**
     * @return \app\defaultClass\PageData
     */
    public function pageData ();
    /**
     * @return \app\defaultClass\Data
     */
    public function data ();
    /**
     * @return \app\defaultClass\View
     */
    public function view ();
    /**
     * @return array $GLOBALS
     */
    public function globals ();
    /**
     * @return  $_SERVER
     */
    public function server ();

} 